import pandas

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df = df.drop_duplicates() #skoro są duplikaty to usuwam aby nie zakłamywały wyniku
df['DateOrder'] = pandas.to_datetime(df['DateOrder'] + ' ' + df['TimeOrder'])
df['DayNight'] = df['DateOrder'].apply(lambda x: 'Night' if (x.hour >= 23 or x.hour < 7) else 'Day')
df_2022 = df[(df['DateOrder'].dt.year == 2022)]
result = df_2022.groupby([df_2022['DateOrder'].dt.strftime('%Y-%m'), 'OrderChannel', 'OrderRegion', 'DayNight']).agg({'OrderValue': 'sum', 'OrderID': 'count'}).reset_index()
result.columns = ['Month', 'OrderChannel', 'OrderRegion', 'DayNight', 'SumOrderValue', 'Transactions']
