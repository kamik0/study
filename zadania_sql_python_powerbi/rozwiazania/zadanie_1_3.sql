WITH CTE
AS
(
	select kontrahenci.nazwa, sum(faktury.wartosc_netto) as najmn_suma
	from kontrahenci join transakcje on kontrahenci.id=transakcje.kontrahent_id join faktury on transakcje.faktura_id=faktury.id join slownik on kontrahenci.sektor_id=slownik.id
	where slownik.nazwa = 'transport' and transakcje.rodzaj = 1
	group by kontrahenci.nazwa

)
Select TOP 10 cte.nazwa, najmn_suma
From CTE
where najmn_suma < 10 * (select min(najmn_suma) from CTE)