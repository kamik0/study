import pandas

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df = df.drop_duplicates()
df = df.dropna(subset=['OrderValue', 'COGS'], how='all') #polecenie ' bez danych o wartości sprzedaży i COGS – w tym przypadku usuń zduplikowane rekordy' rozumiem tutaj jako usunięcie wierszy w których wartosci obu kolumn są puste
df['COGS'] = df['COGS'].str.replace(' ', '').str.replace(',', '.').astype(float) #ujednolicenie zapisu w tej kolumnie
df['COGS'].fillna(df['OrderValue'] * 0.85, inplace=True)
