import pandas

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df['DateOrder'] = pandas.to_datetime(df['DateOrder'])
channel_list = df['OrderChannel'].unique()

for channel in channel_list:
    channel_2022 = df[(df['DateOrder'].dt.year == 2022) & (df['OrderChannel'] == channel)]
    channel_2022_sort = channel_2022.sort_values(by=['OrderRegion', 'DateOrder', 'TimeOrder'])
    channel_2022_sort.to_csv(f"channel{channel}.csv", sep=';')
