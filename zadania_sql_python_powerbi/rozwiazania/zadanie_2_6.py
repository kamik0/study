import pandas
import matplotlib.pyplot as plt

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df = df.drop_duplicates() #skoro są duplikaty to usuwam aby nie zakłamywały wyniku
df['DateOrder'] = pandas.to_datetime(df['DateOrder'])
df_2022 = df[(df['DateOrder'].dt.year == 2022)]
sum_month = df_2022.groupby(df_2022['DateOrder'].dt.strftime('%Y-%m'))['OrderValue'].sum()

plt.figure(figsize=(10, 6))
sum_month.plot(kind='bar', color='skyblue')
plt.ticklabel_format(style='plain', axis='y')
plt.xlabel('Miesiąc')
plt.ylabel('Suma Wartości Sprzedaży')
plt.title('Suma Wartości Sprzedaży w Poszczególnych Miesiącach 2022')
plt.xticks(rotation=45)
plt.show()