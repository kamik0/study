import pandas

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df['COGS'] = df['COGS'].str.replace(' ', '').str.replace(',', '.').astype(float)
df['Margin'] = df['OrderValue'] - df['COGS']

df['DateOrder'] = pandas.to_datetime(df['DateOrder'])
df['CumMargin'] = df.groupby(df['DateOrder'].dt.year)['Margin'].cumsum()

suma_2021 = df[df['DateOrder'].dt.year == 2021]['Margin'].sum()
suma_2022 = df[df['DateOrder'].dt.year == 2022]['Margin'].sum()

KPI = 40000000

KPI_day_2021 = df[df['DateOrder'].dt.year == 2021].loc[df['CumMargin'] > KPI, 'DateOrder'].min()
KPI_day_2022 = df[df['DateOrder'].dt.year == 2022].loc[df['CumMargin'] > KPI, 'DateOrder'].min()

if suma_2021 >= KPI:
    print('W 2021 sklep osiągnął oczekiwany KPI w wysokoci', KPI, 'uzyskując wynik:', suma_2021, 'Wynik został osiągnięty dnia:', KPI_day_2021.day, 'miesiąca', KPI_day_2021.month)
else:
    print('W 2021 sklep nie osiągnął oczekiwanego KPI w wysokoci', KPI, 'uzyskując wynik:', suma_2021)

if suma_2022 >= KPI:
    print('W 2022 sklep osiągnął oczekiwany KPI w wysokoci', KPI, 'uzyskując wynik:', suma_2022, 'Wynik został osiągnięty dnia:', KPI_day_2022.day, 'miesiąca', KPI_day_2022.month)
else:
    print('W 2022 sklep nie osiągnął oczekiwanego KPI w wysokoci', KPI, 'uzyskując wynik:', suma_2022)