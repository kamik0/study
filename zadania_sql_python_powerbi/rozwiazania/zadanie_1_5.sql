WITH CTE
AS
(
	select slownik.nazwa as 'Nazwa produktu', FORMAT(faktury.data_sprzedazy, 'MMMM', 'pl-PL') as 'Miesi�c sprzeda�y', sum(wartosc_netto) as 'Sprzeda�', month(faktury.data_sprzedazy) as 'nr_miesiac'
	from transakcje join faktury on transakcje.faktura_id=faktury.id join slownik on transakcje.produkt_id=slownik.id
	where Year(faktury.data_sprzedazy) = 2022
	group by transakcje.produkt_id, slownik.nazwa, FORMAT(faktury.data_sprzedazy, 'MMMM', 'pl-PL'), month(faktury.data_sprzedazy)
)
select [Nazwa produktu], [Miesi�c sprzeda�y], round(Sprzeda�,1) as Sprzeda�
from CTE
order by nr_miesiac asc, Sprzeda� desc