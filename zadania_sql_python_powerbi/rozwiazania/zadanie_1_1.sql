select top 3 kontrahenci.nazwa, CEILING(sum(wartosc_netto)) as suma_netto, kontrahenci.region_id
from kontrahenci join transakcje on kontrahenci.id=transakcje.kontrahent_id join faktury on transakcje.faktura_id=faktury.id
where rodzaj = 1 and YEAR(data_sprzedazy) = '2022' 
group by kontrahenci.nazwa, kontrahenci.region_id
order by suma_netto desc
