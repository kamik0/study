import pandas

df = pandas.read_csv(r'C:\Users\Kamil\Desktop\Dane.csv', sep=';')
df['COGS'] = df['COGS'].str.replace(' ', '').str.replace(',', '.').astype(float)
df['Margin'] = df['OrderValue'] - df['COGS']