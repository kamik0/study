#!/usr/bin/python
import pandas
import codecs
import pymysql.cursors

keyboard = input("Jaka nazwa pliku xlsx na pulpicie?:")
keyboard2 = input("Jaka nazwa arkusza?:")
id_shopa = input("Jaki jest id shopa?:")
excel_data_df = pandas.read_excel(f"C:/Users/k.kozlowski/desktop/{keyboard}.xlsx", sheet_name=keyboard2)
view = excel_data_df.iloc[:, [0, 1, 2, 3, 4, 5, 7]]
output = ''
i = 1
# lista krajow z dlugimi kodami produktow
lst = ['EN', 'PL', 'IT', 'CZ', 'ES', 'PT', 'RO', 'FR', 'SK', 'GR', 'HU', 'HR', 'DE', 'AT', 'SI', 'BG', 'SE']
to_check = []
for row in view.iterrows():
    # jak cena konczy sie źle np 129,0 zamiast 129
    if isinstance(row[1][4], float):
        if (row[1][4]).is_integer():
            row[1][4] = int(row[1][4])
    # jak jest dlugi kod produktu to sprawdza czy ostatnie znaki sa na liscie krajow i dodaje kod kraju do nazwy produktu
    row[1][1] = row[1][1].strip() #usuwa spacje
    if len(row[1][1]) == 5 and row[1][1][-2:] in lst:
        code = row[1][1]
        row[1][2] = row[1][2] + " " + code[-2:]

    output += f'{row[1][6]},{i},NIE,Standalone,Prywatna,{row[1][0]},{row[1][2]},{row[1][3]},szt,{row[1][4]},{row[1][5]},Pozostałe,null,null \n'
    i += 1
    to_check.append({'kraj': str.lower(row[1][0]), 'produkt': row[1][2]})
# Test 1U,1,NIE,Standalone,Prywatna,Belgia,ProduktX,1,szt,57,EUR,Pozostałe,null,null
print(output)
file = codecs.open(f"C:/Users/k.kozlowski/desktop/{keyboard}.csv", "w", "utf-8")
file.write(output)
file.close()
print(f"Utworzono plik: {keyboard}.csv")

connection = pymysql.connect(host='baza-cc-mariadb-slave-01.com',
                             user='k.kozlowski',
                             password='QazWsx123456',
                             database='bazacc',
                             cursorclass=pymysql.cursors.DictCursor)
try:
    with connection.cursor() as cursor:
        cursor.execute("SELECT products.product_name, product_code, countries.name FROM bazacc.products_packages " \
              "left join bazacc.product_segmentation on bazacc.product_segmentation.product_id = bazacc.products_packages.product_id " \
              "join bazacc.products on bazacc.products_packages.product_id = bazacc.products.product_id " \
              "join bazacc.countries	on bazacc.countries.country_id = bazacc.products_packages.country_id " \
              "where products.cc_customer_id = (%s) group by product_name, countries.name", id_shopa)
        result = cursor.fetchall()

    for produktwsad in to_check:
      niemaprod = True
      kraj = produktwsad['kraj']
      produkt = produktwsad['produkt']
      for produktbaza in result:
        if str.lower(produktbaza['name']) == kraj:
          if produkt == produktbaza['product_name']:
            niemaprod = False
      if niemaprod == True:
        print('w shopie o id', {id_shopa},' nie ma produktu: "', produktwsad['produkt'], '" w kraju: ', produktwsad['kraj'])
except Exception as ex:
    print('nie mozna polaczyc z baza, ', ex)
finally:
    connection.close()
    print('produkty są skonfigurowane w baza w shopie o id ', id_shopa)
