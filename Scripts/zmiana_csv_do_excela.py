import pandas
import openpyxl
import os

check = input("Czy plik csv jest podzielony na patrie? (y/n):")
if check == 'y':
    keyboard = input("Jakie sa nazwy pakolejnych partii? (oddzielone przecinkami, bez rozszerzenia):")
    kblst = keyboard.split(",")
    merged = pandas.DataFrame()
    for file in kblst:
        data = pandas.read_csv(f'C:/Users/k.kozlowski/desktop/{file}.csv', header=None, delimiter=',')
        merged = pandas.concat([merged, data], ignore_index=True, axis=0)
    merged.to_csv('C:/Users/k.kozlowski/desktop/csvZestawPakiet.csv', index=False, header=None)
    print('złączono pliki', kblst)

    df = merged.iloc[:, [0, 5, 6, 7, 8, 9, 10, 14, 2, 15]]
    df.columns = ['Nazwa pakietu', 'Kraj', 'Nazwa produktu', 'Sztuk', 'Jednostka', 'Cena', 'Waluta',
                  'Numer pakietu', 'Czy zestaw', 'Numer zestawu']
    for index, row in df.iterrows():
        if row['Czy zestaw'] != 'TAK':
            df.at[index, 'Numer zestawu'] = ''
    if os.path.isfile('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx'):
        print("plik dodane_pakiety.xlsx juz jest na pulpicie nie bede go nadpisywac")
    else:
        df.to_excel(r'C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx', index=None, header=None)

        # auto szerokosc kolumn
        writer = pandas.ExcelWriter('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
        df.to_excel(writer, sheet_name='dodane_pakiety', index=None)
        # Auto-adjust columns' width +1
        for column in df:
            column_width = (max(df[column].astype(str).map(len).max(), len(str(column))) + 1)
            col_idx = df.columns.get_loc(column)
            writer.sheets['dodane_pakiety'].set_column(col_idx, col_idx, column_width)
        writer.save()

        wb = openpyxl.load_workbook(filename='C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
        tab = openpyxl.worksheet.table.Table(displayName="df",
                                             ref=f'A1:{openpyxl.utils.get_column_letter(df.shape[1])}{len(df) + 1}')
        wb['dodane_pakiety'].add_table(tab)
        wb.save('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
else:
    read_file = pandas.read_csv(r'C:/Users/k.kozlowski/desktop/csvZestawPakiet.csv', header=None)
    df = read_file.iloc[:, [0, 5, 6, 7, 8, 9, 10, 14, 2, 15]]
    df.columns = ['Nazwa pakietu', 'Kraj', 'Nazwa produktu', 'Sztuk', 'Jednostka', 'Cena', 'Waluta', 'Numer pakietu', 'Czy zestaw', 'Numer zestawu']
    for index, row in df.iterrows():
        if row['Czy zestaw'] != 'TAK':
            df.at[index, 'Numer zestawu'] = ''
    if os.path.isfile('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx'):
        print("plik dodane_pakiety.xlsx juz jest na pulpicie nie bede go nadpisywac")
    else:
        df.to_excel(r'C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx', index=None, header=None)

        # auto szerokosc kolumn
        writer = pandas.ExcelWriter('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
        df.to_excel(writer, sheet_name='dodane_pakiety', index=None)
        # Auto-adjust columns' width +1
        for column in df:
            column_width = (max(df[column].astype(str).map(len).max(), len(str(column))) + 1)
            col_idx = df.columns.get_loc(column)
            writer.sheets['dodane_pakiety'].set_column(col_idx, col_idx, column_width)
        writer.save()

        wb = openpyxl.load_workbook(filename='C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
        tab = openpyxl.worksheet.table.Table(displayName="df", ref=f'A1:{openpyxl.utils.get_column_letter(df.shape[1])}{len(df) + 1}')
        wb['dodane_pakiety'].add_table(tab)
        wb.save('C:/Users/k.kozlowski/desktop/dodane_pakiety.xlsx')
