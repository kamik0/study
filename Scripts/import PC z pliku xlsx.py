import pandas as pd
import codecs
import numpy as np
import pymysql.cursors


def country(code):
    if code == 'AUT':
        return 'Austria'
    if code == 'AT':
        return 'Austria'
    if code == 'BEL':
        return 'Belgia'
    if code == 'BE':
        return 'Belgia'
    if code == 'BGR':
        return 'Bułgaria'
    if code == 'BG':
        return 'Bułgaria'
    if code == 'BRA':
        return 'Brazylia'
    if code == 'CZE':
        return 'Czechy'
    if code == 'CZ':
        return 'Czechy'
    if code == 'DEU':
        return 'Niemcy'
    if code == 'DE':
        return 'Niemcy'
    if code == 'ESP':
        return 'Hiszpania'
    if code == 'ES':
        return 'Hiszpania'
    if code == 'FIN':
        return 'Finlandia'
    if code == 'FRA':
        return 'Francja'
    if code == 'FR':
        return 'Francja'
    if code == 'GRE':
        return 'Grecja'
    if code == 'GR':
        return 'Grecja'
    if code == 'HRV':
        return 'Chorwacja'
    if code == 'HR':
        return 'Chorwacja'
    if code == 'HUN':
        return 'Węgry'
    if code == 'HU':
        return 'Węgry'
    if code == 'ITA':
        return 'Włochy'
    if code == 'IT':
        return 'Włochy'
    if code == 'MEX':
        return 'Meksyk'
    if code == 'NLD':
        return 'Holandia'
    if code == 'POL':
        return 'Polska'
    if code == 'PL':
        return 'Polska'
    if code == 'PRT':
        return 'Portugalia'
    if code == 'PT':
        return 'Portugalia'
    if code == 'ROM':
        return 'Rumunia'
    if code == 'RO':
        return 'Rumunia'
    if code == 'SLO':
        return 'Słowenia'
    if code == 'SI':
        return 'Słowenia'
    if code == 'SVK':
        return 'Słowacja'
    if code == 'SK':
        return 'Słowacja'
    if code == 'SWE':
        return 'Szwecja'
    if code == 'SE':
        return 'Szwecja'


def unique(list1):
    x = np.array(list1)
    return np.unique(x)


def float2int(x):#zmiana cen np z 45.0 na 45 bo bundler nie wciaga z koncowka .0
    if isinstance(x, float):
        if x.is_integer():
            return int(x)
    else:
        return x


# lista krajow z dlugimi 5 znakowymi kodami produktow
lst = ['EN', 'PL', 'IT', 'CZ', 'ES', 'PT', 'RO', 'FR', 'SK', 'GR', 'HU', 'HR', 'DE', 'AT', 'SE', 'BG', 'SI']

pd.set_option('display.max_columns', None)
keyboard = input("PC standalone(s) czy zestawy(z)?")
id_shopa = input("Jaki jest id shopa?:")
to_check = []
if keyboard == 's':
    excel_data_df = pd.read_excel("C:/Users/k.kozlowski/desktop/PC puliczne do dodania.xlsx", sheet_name='standalone')
    view = excel_data_df.iloc[:, [0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11]]
    pclist = unique(view['PC'].tolist())
    output = ''
    service = False
    view.round(1) # zaokraglenie wszystkich kolumn do inta
    i = 1
    for row in view.iterrows():    # tu sie mieszcza tuple a w kazdej tupli znajduje sie typ pandas series (naglowek + wiersz)
        # print('kod kraju: ', row[1][0])    # kod kraju
        # print('nazwa produktu: ', row[1][1])    # nazwa produktu
        # print('kod produktu: ', row[1][2])    # kod produktu
        # print('waluta: ', row[1][3])    # waluta
        # print('karalogowa: ', row[1][4])    # cena katalogowa
        # print('standard: ', row[1][5])    # cena standard
        # print('upsell: ', row[1][6])    # cena upsell
        # print('exit: ', row[1][7])    # cena exit
        # print('upsell2: ', row[1][8])    # oplata upsell2
        # print('serwisowa: ', row[1][9]) # oplata serwisowa
        # print('polityka', row[1][10])   # nr polityki

        # jak jest dlugi kod produktu to sprawdza czy ostatnie znaki sa na liscie krajow i dodaje kod kraju do nazwy produktu
        row[1][2] = row[1][2].strip()  # usuwa spacje
        if len(row[1][2]) == 5 and row[1][2][-2:] in lst:
            code5 = row[1][2]
            row[1][1] = row[1][1] + " " + code5[-2:]
        output += f'{row[1][1]} {row[1][10]} Standard,{i},NIE,Standalone,Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{row[1][5]},{row[1][3]},Pozostałe,null,null \n'
        i += 1
        output += f'{row[1][1]} {row[1][10]} Exit,{i},NIE,Standalone,Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{row[1][7]},{row[1][3]},Pozostałe,null,null \n'
        i += 1
        output += f'{row[1][1]} {row[1][10]} Upsell,{i},NIE,Standalone,Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{row[1][6]},{row[1][3]},Pozostałe,null,null \n'
        i += 1
        if row[1][8] != '-' or '':
            output += f'{row[1][1]} {row[1][10]} Upsell2,{i},NIE,Standalone,Publiczna,{country(row[1][0])},{row[1][1]},2,szt,{row[1][8]},{row[1][3]},Pozostałe,null,null \n'
            i += 1
        if row[1][9] != '-' or '':
            service = True
        to_check.append({'kraj': str.lower(country(row[1][0])), 'produkt': row[1][1]})
        file = codecs.open(f"C:/Users/k.kozlowski/desktop/wsad PC standalone {pclist}.csv", "w", "utf-8")
        file.write(output)
        file.close()
    print(output)
    print(f"Utworzono plik wsad PC {pclist}.csv")

    if service is True:
        print('dodaj jeszcze recznie oplate serwisowa!')

if keyboard == 'z':
    excel_data_df = pd.read_excel("C:/Users/k.kozlowski/desktop/PC puliczne do dodania.xlsx", sheet_name='zestawy')
    view = excel_data_df.iloc[:, [0, 1, 2, 3, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23]]
    pclist = unique(view['ID PC'].tolist())
    output = ''
    # zaokraglanie cen ( 2 - do dwoch miejsc po przecinku, 0 - bez przecinkow)
    view = view.round({'Cena standard Pakiet': 0, 'Cena standard Produkt': 0,'Cena standard Książka': 0,
                'Cena upsell Pakiet': 0,'Cena upsell Produkt': 0,'Cena upsell Książka': 0,
                'Cena exit Pakiet': 0,'Cena exit Produkt': 0,'Cena exit Książka': 0,
                'Cena upsell 2 Pakiet': 0,'Cena upsell 2 Produkt': 0,'Cena upsell 2 Książka': 0,})
    #sprawdzanie czy cena produktu + ksiazki = cena zestawu
    badrows = []
    i = 1
    for row in view.iterrows():     # tu sie mieszcza tuple a w kazdej tupli znajduje sie typ pandas series (naglowek + wiersz)
        if row[1][6] + row[1][7] == row[1][5] and row[1][12] + row[1][13] == row[1][11] and row[1][9] + row[1][10] == row[1][8] and row[1][15] + row[1][16] == row[1][14]:
            row[1][2] = row[1][2].strip()  # usuwa spacje w kodzie produktu
            row[1][3] = row[1][3].strip()  # usuwa spacje w kodzie produktu
            if len(row[1][2]) == 5 and row[1][2][-2:] in lst:
                code5 = row[1][2]
                row[1][1] = row[1][1] + " " + code5[-2:]  # dopisuje kod kraju do nazwy produktu
            if len(row[1][3]) == 5 and row[1][3][-2:] in lst:
                code52 = row[1][3]
                row[1][18] = row[1][18] + " " + code52[-2:]  # dopisuje kod kraju do nazwy ksiazki
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Standard,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][18]},1,szt,{float2int(row[1][7])},{row[1][4]},Pozostałe,null,null \n'
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Standard,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{float2int(row[1][6])},{row[1][4]},Pozostałe,null,null \n'
            i += 1
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Exit,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][18]},1,szt,{float2int(row[1][13])},{row[1][4]},Pozostałe,null,null \n'
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Exit,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{float2int(row[1][12])},{row[1][4]},Pozostałe,null,null \n'
            i += 1
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Upsell,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][18]},1,szt,{float2int(row[1][10])},{row[1][4]},Pozostałe,null,null \n'
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Upsell,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][1]},1,szt,{float2int(row[1][9])},{row[1][4]},Pozostałe,null,null \n'
            i += 1
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Upsell2,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][18]},1,szt,{float2int(row[1][16])},{row[1][4]},Pozostałe,null,null \n'
            output += f'{row[1][19]} {row[1][1]} PC {row[1][17]} Upsell2,{i},TAK,{row[1][19]},Publiczna,{country(row[1][0])},{row[1][1]},2,szt,{float2int(row[1][15])},{row[1][4]},Pozostałe,null,null \n'
            i += 1
            to_check.append({'kraj': str.lower(country(row[1][0])), 'produkt': row[1][1]})
        else:
            badrows.append(row)

    if len(badrows) > 0:
        (print('nie zgadzają się sumy cen w wierszach:\n', badrows))
    else:
        print(output)
        file = codecs.open(f"C:/Users/k.kozlowski/desktop/wsad PC zestawy {pclist}.csv", "w", "utf-8")
        file.write(output)
        file.close()
        print(f"Utworzono plik wsad PC zestawy {pclist}.csv")

connection = pymysql.connect(host='baza-cc-mariadb-slave-01.com',
                             user='k.kozlowski',
                             password='QazWsx123456',
                             database='bazacc',
                             cursorclass=pymysql.cursors.DictCursor)
try:
    with connection.cursor() as cursor:
        cursor.execute("SELECT products.product_name, product_code, countries.name FROM bazacc.products_packages " \
              "left join bazacc.product_segmentation on bazacc.product_segmentation.product_id = bazacc.products_packages.product_id " \
              "join bazacc.products on bazacc.products_packages.product_id = bazacc.products.product_id " \
              "join bazacc.countries	on bazacc.countries.country_id = bazacc.products_packages.country_id " \
              "where products.cc_customer_id = (%s) group by product_name, countries.name", id_shopa)
        result = cursor.fetchall()

    for produktwsad in to_check:
      niemaprod = True
      kraj = produktwsad['kraj']
      produkt = produktwsad['produkt']
      for produktbaza in result:
        if str.lower(produktbaza['name']) == kraj:
          if produkt == produktbaza['product_name']:
            niemaprod = False
      if niemaprod == True:
        print('w shopie o id', {id_shopa},' nie ma produktu: "', produktwsad['produkt'], '" w kraju: ', produktwsad['kraj'])
except Exception as ex:
    print('nie mozna polaczyc z baza, ', ex)
finally:
    connection.close()
    print('produkty są skonfigurowane w baza w shopie o id ', id_shopa)
